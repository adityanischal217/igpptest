package com.aditya.test.igpp.ui.home.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.aditya.test.igpp.BR;
import com.aditya.test.igpp.R;
import com.aditya.test.igpp.ui.home.model.ListItem;
import com.aditya.test.igpp.ui.web.WebActivity;

import java.util.List;

public class GiftCustomAdapter
        extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_CAKE = 0;
    private static final int TYPE_FLOWER = 1;
    private List<ListItem> mResponseModel;
    Context context;


    public GiftCustomAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ViewDataBinding binding;
        switch (i) {
            case TYPE_CAKE:
                binding = DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()),
                        R.layout.single_list_item1, viewGroup, false);

                return new DeveloperViewHolder(binding);
            case TYPE_FLOWER:
                binding = DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()),
                        R.layout.single_list_item2, viewGroup, false);

                return new DeveloperViewHolder1(binding);

        }
        return null;


    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder mDeveloperViewHolder, int i) {
        ListItem dataModel;
        switch (mDeveloperViewHolder.getItemViewType()) {
            case 0:
            case 3:
                DeveloperViewHolder viewHolder0 = (DeveloperViewHolder) mDeveloperViewHolder;
                dataModel = mResponseModel.get(i);
                viewHolder0.bind(dataModel);
                break;
            case 1:
            case 2:
                DeveloperViewHolder1 viewHolder2 = (DeveloperViewHolder1) mDeveloperViewHolder;
                dataModel = mResponseModel.get(i);
                viewHolder2.bind1(dataModel);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (mResponseModel != null) {
            return mResponseModel.size();
        } else {
            return 0;
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setDeveloperList(List<ListItem> mDeveloperModel) {
        this.mResponseModel = mDeveloperModel;
        notifyDataSetChanged();


    }

    class DeveloperViewHolder extends RecyclerView.ViewHolder {

        public ViewDataBinding itemRowBinding;

        public DeveloperViewHolder(ViewDataBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;

        }

        public void bind(ListItem obj) {
            itemRowBinding.setVariable(BR.developerModel, obj);
            itemRowBinding.executePendingBindings();
            itemRowBinding.getRoot().setOnClickListener(view -> {
                Toast.makeText(context, "Redirecting to : " + obj.getUrl(),
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtra("imageURL", obj.getUrl());
                context.startActivity(intent);
            });
        }


    }

    class DeveloperViewHolder1 extends RecyclerView.ViewHolder {

        public ViewDataBinding itemRowBinding;

        public DeveloperViewHolder1(ViewDataBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;


        }


        public void bind1(ListItem obj) {
            itemRowBinding.setVariable(BR.developerModela, obj);
            itemRowBinding.executePendingBindings();
            itemRowBinding.getRoot().setOnClickListener(view -> {
                Toast.makeText(context, "Redirecting to : " + obj.getUrl(),
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtra("imageURL", obj.getUrl());
                context.startActivity(intent);
            });
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return TYPE_CAKE;
        }
        if (position == 1) {
            return TYPE_FLOWER;
        }
        if (position == 2) {
            return TYPE_FLOWER;
        }
        if (position == 3) {
            return TYPE_CAKE;
        }

        return TYPE_CAKE;
    }

}

