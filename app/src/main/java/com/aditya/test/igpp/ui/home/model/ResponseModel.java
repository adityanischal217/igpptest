
package com.aditya.test.igpp.ui.home.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResponseModel implements Serializable {

    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("visibility")
    @Expose
    private Boolean visibility;
    @SerializedName("is_section_title")
    @Expose
    private Boolean isSectionTitle;
    @SerializedName("section_title")
    @Expose
    private String sectionTitle;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("layout")
    @Expose
    private String layout;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    /**
     * No args constructor for use in serialization
     */
    public ResponseModel() {
    }


    public ResponseModel(Integer order, Boolean visibility, Boolean isSectionTitle, String sectionTitle, String type, String layout, List<Datum> data) {
        super();
        this.order = order;
        this.visibility = visibility;
        this.isSectionTitle = isSectionTitle;
        this.sectionTitle = sectionTitle;
        this.type = type;
        this.layout = layout;
        this.data = data;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }

    public Boolean getIsSectionTitle() {
        return isSectionTitle;
    }

    public void setIsSectionTitle(Boolean isSectionTitle) {
        this.isSectionTitle = isSectionTitle;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}

