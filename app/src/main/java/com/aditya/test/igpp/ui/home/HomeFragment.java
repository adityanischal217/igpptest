package com.aditya.test.igpp.ui.home;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aditya.test.igpp.ui.home.adapter.CakeCustomAdapter;
import com.aditya.test.igpp.ui.home.adapter.FlowerCustomAdapter;
import com.aditya.test.igpp.ui.home.adapter.GiftCustomAdapter;
import com.aditya.test.igpp.ui.home.model.ListItem;
import com.aditya.test.igpp.databinding.FragmentHomeBinding;

import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    ProgressBar loadBar;
    public FragmentHomeViewModel mainViewModel;
    public CakeCustomAdapter mCake_CustomAdapter;
    public FlowerCustomAdapter mFlower_CustomAdapter;
    public GiftCustomAdapter mGift_CustomAdapter;
    TextView textViewCake, textViewFlower, textViewGift, viewAll, viewAll1, viewAll2;
    RecyclerView recyclerViewcake, recyclerViewFlower, recyclerViewGift;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mainViewModel =
                new ViewModelProvider(this).get(FragmentHomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        // bind
        recyclerViewcake = binding.viewdeveloper;
        recyclerViewFlower = binding.view1developer;
        recyclerViewGift = binding.view2developer;
        loadBar = binding.loadBar;
        textViewCake = binding.textViewCakes;
        textViewFlower = binding.textViewFlower;
        textViewGift = binding.textViewGift;
        viewAll = binding.viewAll;
        viewAll1 = binding.viewAll2;
        viewAll2 = binding.viewAll3;
        viewAll.setOnClickListener(view -> Toast.makeText(getContext(), "This functionality is coming soon",
                Toast.LENGTH_LONG).show());
        viewAll1.setOnClickListener(view -> Toast.makeText(getContext(), "This functionality is coming soon",
                Toast.LENGTH_LONG).show());
        viewAll2.setOnClickListener(view -> Toast.makeText(getContext(), "This functionality is coming soon",
                Toast.LENGTH_LONG).show());
        // Create a grid layout with two columns
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);

        // Create a custom SpanSizeLookup where the first item spans both columns
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });
        // Create a grid layout with two columns
        GridLayoutManager layoutManager1 = new GridLayoutManager(getContext(), 2);

        // Create a custom SpanSizeLookup where the first item spans both columns
        layoutManager1.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 4 ? 2 : 1;
            }
        });


        recyclerViewcake.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerViewcake.setHasFixedSize(true);
        recyclerViewFlower.setLayoutManager(layoutManager);
        recyclerViewFlower.setHasFixedSize(true);
        recyclerViewGift.setLayoutManager(layoutManager1);
        recyclerViewGift.setHasFixedSize(true);
        //init the  adataper
        mCake_CustomAdapter = new CakeCustomAdapter(getContext());
        mFlower_CustomAdapter = new FlowerCustomAdapter(getContext());
        mGift_CustomAdapter = new GiftCustomAdapter(getContext());
        //set the Adapter
        recyclerViewcake.setAdapter(mCake_CustomAdapter);
        recyclerViewFlower.setAdapter(mFlower_CustomAdapter);
        recyclerViewGift.setAdapter(mGift_CustomAdapter);

        getAllDev();
        return root;
    }

    @SuppressLint("SetTextI18n")
    private void getAllDev() {
        //get the list of dev
        mainViewModel.getAllDeveloper().observe(this, mDeveloperModel -> {
            HashMap<String, List<ListItem>> res = new HashMap<>();
            if (mDeveloperModel != null) {
                for (int i = 0; i < mDeveloperModel.size(); ++i) {
                    res.put(mDeveloperModel.get(i).getShowcaseName(), mDeveloperModel.get(i).getList());
                }

                if (res.containsKey("Cakes")) {
                    textViewCake.setText("Cakes");
                    mCake_CustomAdapter.setDeveloperList(res.get("Cakes"));
                    loadBar.setVisibility(View.GONE);

                }
                if (res.containsKey("Flowers")) {
                    textViewFlower.setText("Flowers");
                    mFlower_CustomAdapter.setDeveloperList(res.get("Flowers"));
                    loadBar.setVisibility(View.GONE);

                }
                if (res.containsKey("Personalized Gifts")) {
                    textViewGift.setText("Personalized Gifts");
                    mGift_CustomAdapter.setDeveloperList(res.get("Personalized Gifts"));
                    loadBar.setVisibility(View.GONE);

                }
            }

        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}