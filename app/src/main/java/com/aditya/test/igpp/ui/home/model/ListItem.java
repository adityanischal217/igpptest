package com.aditya.test.igpp.ui.home.model;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.aditya.test.igpp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import kotlin.jvm.JvmStatic;

public class ListItem implements Serializable {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("title")
    @Expose
    private String title;

    public ListItem() {
    }


    public ListItem(String url, String image, String title) {
        super();
        this.url = url;
        this.image = image;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @BindingAdapter({"image"})
    public static void loadImage(ImageView imageView, String imageURL) {

        Glide.with(imageView.getContext())
                .load(imageURL)
                .placeholder(R.drawable.ic_launcher_foreground)
                .dontAnimate()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .into(imageView);
    }

   /* @BindingAdapter({"title"})
    public static void setBlur(TextView view, String title) {
        Blurred.with(view.getContext())
                .load(view)
                .blur(TextViewBlur.BlurMode.OUTSIDE, 20);
    }*/


}
