package com.aditya.test.igpp.ui.home.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Datum implements Serializable {

    @SerializedName("showcase_url")
    @Expose
    private String showcaseUrl;
    @SerializedName("showcase_name")
    @Expose
    private String showcaseName;
    @SerializedName("list")
    @Expose
    private List<ListItem> list = null;

    /**
     * No args constructor for use in serialization
     */
    public Datum() {
    }


    public Datum(String showcaseUrl, String showcaseName, List<ListItem> list) {
        super();
        this.showcaseUrl = showcaseUrl;
        this.showcaseName = showcaseName;
        this.list = list;
    }

    public String getShowcaseUrl() {
        return showcaseUrl;
    }

    public void setShowcaseUrl(String showcaseUrl) {
        this.showcaseUrl = showcaseUrl;
    }

    public String getShowcaseName() {
        return showcaseName;
    }

    public void setShowcaseName(String showcaseName) {
        this.showcaseName = showcaseName;
    }

    public List<ListItem> getList() {
        return list;
    }

    public void setList(List<ListItem> list) {
        this.list = list;
    }

}
