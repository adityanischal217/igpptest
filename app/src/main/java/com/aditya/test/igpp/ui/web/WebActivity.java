package com.aditya.test.igpp.ui.web;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.aditya.test.igpp.R;
import com.aditya.test.igpp.databinding.ActivityWebBinding;

public class WebActivity extends AppCompatActivity {

    WebViewModel webViewModel;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityWebBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_web);
        setContentView(binding.getRoot());
        Intent intent = getIntent();
        String url = intent.getStringExtra("imageURL");
        webViewModel = new ViewModelProvider(this).get(WebViewModel.class);
        webViewModel.setUrl(url);
        final WebView webView = binding.webView;
        final ProgressBar progressBar = binding.progressBar;
        webViewModel.getUrl().observe(this, url1 -> {
            webView.loadUrl(url1);
            // this will enable the javascript.
            webView.getSettings().setJavaScriptEnabled(true);

            // WebViewClient allows you to handle
            // onPageFinished and override Url loading.
            webView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url1) {
                    view.loadUrl(url1);
                    return true;
                }

                public void onPageFinished(WebView view, String url1) {
                    progressBar.setVisibility(View.GONE);

                }

            });

        });
    }
}


