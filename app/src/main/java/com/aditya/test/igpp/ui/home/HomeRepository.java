package com.aditya.test.igpp.ui.home;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.aditya.test.igpp.ui.home.model.Datum;
import com.aditya.test.igpp.R;
import com.aditya.test.igpp.ui.home.model.ResponseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class HomeRepository {

    private static final String TAG = "HomeRepository";
    private final List<Datum> mdataList = new ArrayList<>();
    private final MutableLiveData<List<Datum>> mutableLiveData = new MutableLiveData<>();
    Context context;

    public HomeRepository(Context context) {
        this.context = context;
    }

    //call json file and  retun  MutableLiveData
    public MutableLiveData<List<Datum>> getMutableLiveData() {
        addItemsFromJSON();
        return mutableLiveData;
    }

    private void addItemsFromJSON() {
        try {

            String jsonDataString = readJSONDataFromFile();
            Gson gson = new GsonBuilder().create();
            JsonArray json = new JsonParser().parse(jsonDataString).getAsJsonArray();
            if (json != null) {

                for (int i = 0; i <= json.size() - 1; i++) {
                    try {

                        JsonObject jsonobj = new JsonParser().parse(json.get(i).toString()).getAsJsonObject();
                        ResponseModel responseModel = gson.fromJson(jsonobj, ResponseModel.class);
                        for (int j = 0; j <= responseModel.getData().size() - 1; i++) {
                            List<Datum> responsedata = responseModel.getData();
                            mdataList.add(responsedata.get(i));
                        }


                    } catch (Exception ex) {
                        Log.v(TAG, ex.getMessage());
                    }

                }
                mutableLiveData.setValue(mdataList);
            }


        } catch (IOException e) {
            Log.d(TAG, "addItemsFromJSON: ", e);
        }
    }

    private String readJSONDataFromFile() throws IOException {

        InputStream inputStream = null;
        StringBuilder builder = new StringBuilder();

        try {

            String jsonString;
            inputStream = context.getResources().openRawResource(R.raw.response);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, StandardCharsets.UTF_8));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }

}

