package com.aditya.test.igpp.ui.web;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class WebViewModel extends ViewModel {

    private final String TAG = this.getClass().getSimpleName();
    private final MutableLiveData<String> url = new MutableLiveData<>();

    public void setUrl(String newUrl) {
        Log.i(TAG, "SET URL: " + newUrl);
        url.setValue(newUrl);
    }

    public LiveData<String> getUrl() {
        return url;
    }
}