package com.aditya.test.igpp.ui.home.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.aditya.test.igpp.R;
import com.aditya.test.igpp.ui.web.WebActivity;
import com.aditya.test.igpp.databinding.SingleListItemBinding;
import com.aditya.test.igpp.ui.home.model.ListItem;

import java.util.List;
import java.util.Random;

public class CakeCustomAdapter
        extends RecyclerView.Adapter<CakeCustomAdapter.DeveloperViewHolder> {

    Context context;
    private List<ListItem> mResponseModel;

    public CakeCustomAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public DeveloperViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        SingleListItemBinding mDeveloperListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.single_list_item, viewGroup, false);

        return new DeveloperViewHolder(mDeveloperListItemBinding);

    }

    @Override
    public void onBindViewHolder(@NonNull DeveloperViewHolder mDeveloperViewHolder, int i) {
        ListItem currentStudent;
        Random rnd = new Random();
        int currentColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        mDeveloperViewHolder.mDeveloperListItemBinding.cvdeveloper.setStrokeColor(currentColor);
        mDeveloperViewHolder.mDeveloperListItemBinding.cvdeveloper.setStrokeWidth(3);
        currentStudent = mResponseModel.get(i);
        mDeveloperViewHolder.mDeveloperListItemBinding.setDeveloperModel(currentStudent);
        mDeveloperViewHolder.mDeveloperListItemBinding.getRoot().setOnClickListener(view -> {
            Toast.makeText(context, "Redirecting to : " + currentStudent.getUrl(),
                    Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, WebActivity.class);
            intent.putExtra("imageURL", currentStudent.getUrl());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        if (mResponseModel != null) {
            return mResponseModel.size();
        } else {
            return 0;
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setDeveloperList(List<ListItem> mDeveloperModel) {
        this.mResponseModel = mDeveloperModel;
        notifyDataSetChanged();
    }


    static class DeveloperViewHolder extends RecyclerView.ViewHolder {
        SingleListItemBinding mDeveloperListItemBinding;

        public DeveloperViewHolder(@NonNull SingleListItemBinding mDeveloperListItemBinding) {
            super(mDeveloperListItemBinding.getRoot());
            this.mDeveloperListItemBinding = mDeveloperListItemBinding;
           
        }
    }


}

