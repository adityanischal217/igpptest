package com.aditya.test.igpp.ui.home;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.aditya.test.igpp.ui.home.model.Datum;

import java.util.List;

public class FragmentHomeViewModel extends AndroidViewModel {
    private final HomeRepository mHomeRepository;
    @SuppressLint("StaticFieldLeak")
    Context context;

    public FragmentHomeViewModel(@NonNull Application application) {
        super(application);
        context = getApplication().getApplicationContext();
        mHomeRepository = new HomeRepository(context);

    }

    public LiveData<List<Datum>> getAllDeveloper() {
        return mHomeRepository.getMutableLiveData();
    }
}